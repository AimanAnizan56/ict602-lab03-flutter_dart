import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build (BuildContext context) {
    return MaterialApp(
      title: 'Flutter Lab 3',
      home: Scaffold(
        appBar: AppBar( title: const Text('Aiman bin Anizan') ),
        body: Container(
          margin: const EdgeInsets.fromLTRB(0, 20, 0, 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Column(
                children: const <Widget>[
                  Icon( Icons.phone ),
                  SizedBox(height: 5),
                  Text('CALL'),
                ],
              ),
              Column(
                children: const <Widget>[
                  Icon( Icons.navigation ),
                  SizedBox(height: 5),
                  Text('ROUTE'),
                ],
              ),
              Column(
                children: const <Widget>[
                  Icon( Icons.share ),
                  SizedBox(height: 5),
                  Text('SHARE'),
                ],
              )
            ],
          )
        )
      ),
    );
  }
}